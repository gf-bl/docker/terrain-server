FROM alpine:3.8

LABEL maintainer="Karsten Deininger <karsten.deininger@bl.ch>"

ENV NGINX_VERSION 1.10.1

USER root

RUN apk --update add \
        bash \
        file \
        grep \
        make \
        gcc \
        libc-dev \
        zlib-dev && \
    adduser -D -S -h /home/nginx -s /sbin/nologin -G root --uid 1001 nginx

COPY apply-gz /usr/local/bin
COPY terrain-server.conf /home/nginx/conf/terrain-server.conf
COPY index.html /data/index.html

RUN chmod 755 /usr/local/bin/apply-gz && \
    chown -R nginx:root /home/nginx/conf && \
    chown -R nginx:root /data

USER 1001

RUN mkdir /home/nginx/sbin && \
    mkdir /home/nginx/pid && \
    mkdir /home/nginx/logs && \
    cd /tmp && \
    wget http://nginx.org/download/nginx-$NGINX_VERSION.tar.gz && \
    tar xfz nginx-$NGINX_VERSION.tar.gz && \
    cd nginx-$NGINX_VERSION && \
    ./configure \
        --prefix=/home/nginx \
        --sbin-path=sbin \
        --conf-path=conf/terrain-server.conf \
        --pid-path=pid/nginx.pid \
        --without-http_rewrite_module \
        --without-http_proxy_module \
        --with-http_gzip_static_module \
        --with-http_gunzip_module && \
    make && \
    make install && \
    cd /tmp && \
    rm -rf ./* && \
    chmod -R g+w /home/nginx

USER root

RUN apk del \
        make \
        gcc \
        libc-dev \
        zlib-dev && \
    ln -sf /dev/stdout /home/nginx/logs/access.log && \
	ln -sf /dev/stderr /home/nginx/logs/error.log

USER 1001
WORKDIR /data

EXPOSE 8080

STOPSIGNAL SIGTERM

CMD ["/home/nginx/sbin/nginx", "-g", "daemon off;"]
