# Terrain Server

This is a simple terrain server based on [nginx](http://nginx.org/) for use with the 
[Cesium Web Globe](https://cesiumjs.org/).

To create your terrain tiles, you can use the 
[Cesium Terrain Builder Docker](https://hub.docker.com/r/tumgis/ctb-quantized-mesh/), for example.

## Usage

Run the server:

```
docker run -d --name my-terrain-server -p 8080:8080 -v /path/to/your/tiles:/data registry.gitlab.com/gf-bl/docker/terrain-server:latest
```

Opening http://localhost:8080/ in your browser, should now show the content of your specified 
directory.

## Serve gzipped content

Your terrain tiles may be already compressed on creation. To serve gzipped content, the compressed
files need the _*.gz_ extension. For example, if a compressed tile `1234.terrain` is requested, the
server will serve `1234.terrain.gz` if it is available in the same directory.

To ensure, that all gzipped files have the _*.gz_ extension, you can use an included script on your
running server:

```
docker container exec my-terrain-server apply-gz
```

This script recursively scans all files within your specified directory. If a file is gzipped and the
extension is missing, it will be added.